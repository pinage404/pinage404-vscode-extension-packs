## V0.2.0 / 2023-07-30

-   Replace [Better TOML](https://marketplace.visualstudio.com/items?itemName=bungcip.better-toml) by [Even Better TOML](https://marketplace.visualstudio.com/items?itemName=tamasfe.even-better-toml)
    -   The former is depreciated in favor of the latter.

## v0.1.1 / 2021-12-25

-   Add extension
    -   [ENV](https://marketplace.visualstudio.com/items?itemName=IronGeek.vscode-env) : Adds formatting and syntax highlighting support for env files (`.env`)

## v0.1.0 / 2018-04-01

-   First release
