# Common Configuration Languages VS Code Extension Pack

Collection of extensions for common configuration languages

[![Visual Studio Marketplace Installs][vscode_marketplace_installs]][vscode_marketplace]
[![Open VSX Downloads][open_vsx_downloads]][open_vsx]

## Recommended settings

To use custom `.env` file extension (e.g. `.env.development`) and highlight with [ENV](https://marketplace.visualstudio.com/items?itemName=IronGeek.vscode-env) extension add this to your settings :

```json
{
	"files.associations": {
		".env.*": "env"
	}
}
```

## Want to see more extension added?

Open a [MR][merge-request-url] or an [issue][issue-url] and i will to take a look

[vscode_marketplace_installs]: https://img.shields.io/visual-studio-marketplace/i/pinage404.common-configuration-languages-extension-pack?label=VSCode%20Marketplace%20Installs
[vscode_marketplace]: https://marketplace.visualstudio.com/items?itemName=pinage404.common-configuration-languages-extension-pack
[open_vsx_downloads]: https://img.shields.io/open-vsx/dt/pinage404/common-configuration-languages-extension-pack?label=Open%20VSX%20Registry%20Downloads
[open_vsx]: https://open-vsx.org/extension/pinage404/common-configuration-languages-extension-pack
[merge-request-url]: https://gitlab.com/pinage404/pinage404-vscode-extension-packs/-/merge_requests
[issue-url]: https://gitlab.com/pinage404/pinage404-vscode-extension-packs/-/issues
