# Front End VS Code Extension Pack

Collection of extensions for front end web development (frameworks' agnostic)

[![Visual Studio Marketplace Installs][vscode_marketplace_installs]][vscode_marketplace]
[![Open VSX Downloads][open_vsx_downloads]][open_vsx]

## Recommended settings

To avoid duplicated suggestions, [Path Intellisense](https://marketplace.visualstudio.com/items?itemName=christian-kohler.path-intellisense) recommends to add this to your settings :

```json
{
	"javascript.suggest.paths": false,
	"typescript.suggest.paths": false
}
```

## Want to see more extension added?

Open a [MR][merge-request-url] or an [issue][issue-url] and i will to take a look

[vscode_marketplace_installs]: https://img.shields.io/visual-studio-marketplace/i/pinage404.front-end-extension-pack?label=VSCode%20Marketplace%20Installs
[vscode_marketplace]: https://marketplace.visualstudio.com/items?itemName=pinage404.front-end-extension-pack
[open_vsx_downloads]: https://img.shields.io/open-vsx/dt/pinage404/front-end-extension-pack?label=Open%20VSX%20Registry%20Downloads
[open_vsx]: https://open-vsx.org/extension/pinage404/front-end-extension-pack
[merge-request-url]: https://gitlab.com/pinage404/pinage404-vscode-extension-packs/-/merge_requests
[issue-url]: https://gitlab.com/pinage404/pinage404-vscode-extension-packs/-/issues
