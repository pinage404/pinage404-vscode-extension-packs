## V0.3.0 / 2023-07-30

-   Replace [Better TOML](https://marketplace.visualstudio.com/items?itemName=bungcip.better-toml) by [Even Better TOML](https://marketplace.visualstudio.com/items?itemName=tamasfe.even-better-toml)
    -   The former is depreciated in favor of the latter.

## V0.2.0 / 2022-06-24

-   Replace [Rust](https://marketplace.visualstudio.com/items?itemName=rust-lang.rust) by [`rust-analyzer`](https://marketplace.visualstudio.com/items?itemName=rust-lang.rust-analyzer)
    -   The former is depreciated in favor of the latter.
    -   Thanks for [the recommendation #2](https://gitlab.com/pinage404/pinage404-vscode-extension-packs/-/issues/2) by [@plokid](https://gitlab.com/plokid)

## V0.1.0 / 2021-10-03

-   First release
