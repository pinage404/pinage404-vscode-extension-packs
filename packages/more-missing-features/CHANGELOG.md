## V2.0.0 / 2022-11-15

-   Remove [Local History](https://marketplace.visualstudio.com/items?itemName=xyz.local-history) because [VSCode supports this feature since v1.66 / march 2022](https://code.visualstudio.com/updates/v1_66#_local-history)

## V1.0.0 / 2021-09-19

-   Add icon

## V0.1.0 / 2021-09-18

-   First release
