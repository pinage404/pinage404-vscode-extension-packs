## V2.0.0 / 2022-05-01

-   Replace [cab404's `direnv`](https://marketplace.visualstudio.com/items?itemName=cab404.vscode-direnv) by [Martin Kühl's `direnv`](https://marketplace.visualstudio.com/items?itemName=mkhl.direnv)
    -   The former is depreciated in favor of the latter.
    -   Thanks for [the warning #1](https://gitlab.com/pinage404/pinage404-vscode-extension-packs/-/issues/1) by [@yajoman (Jairo Llopis)](https://gitlab.com/yajoman)

## V1.0.0 / 2021-08-26

-   First release
