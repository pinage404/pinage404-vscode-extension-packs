# Markdown VS Code Extension Pack

Collection of extensions to improve writing document using Markdown

[![Visual Studio Marketplace Installs][vscode_marketplace_installs]][vscode_marketplace]
[![Open VSX Downloads][open_vsx_downloads]][open_vsx]

## Features

### Edition

-   WYSIWYG editor
-   Generate and auto update Table Of Contents (TOC)
-   Paths autocompletion
-   Link update
-   Intelligent pasting
-   Table formatting

### Almost standard Markdown features

-   Task list
-   Footnotes
-   Display Front Matter in the preview
-   Front Matter editor

### Metadata

-   Linting
-   Display linked documents graph
-   Display orphan documents (files without links to them)
-   Display links pointing to a non-existent document
-   Display backlinks
-   Display reading time

### Additionnal features

-   One-click script execution
-   Keyboard shortcuts
-   Better highlight of code snippets
-   and more

## Recommended settings

To display links pointing to a non-existent document, add this to your settings :

```json
{
	"foam.decorations.links.enable": true
}
```

By default, the Table Of Contents (TOC) generated by [Markdown All in One](https://marketplace.visualstudio.com/items?itemName=yzhang.markdown-all-in-one) includes the main title, to avoid that, add this to your settings :

```json
{
	"markdown.extension.toc.levels": "2..6"
}
```

## Want to see more extension added?

Open a [MR][merge-request-url] or an [issue][issue-url] and i will to take a look

[vscode_marketplace_installs]: https://img.shields.io/visual-studio-marketplace/i/pinage404.markdown-extension-pack?label=VSCode%20Marketplace%20Installs
[vscode_marketplace]: https://marketplace.visualstudio.com/items?itemName=pinage404.markdown-extension-pack
[open_vsx_downloads]: https://img.shields.io/open-vsx/dt/pinage404/markdown-extension-pack?label=Open%20VSX%20Registry%20Downloads
[open_vsx]: https://open-vsx.org/extension/pinage404/markdown-extension-pack
[merge-request-url]: https://gitlab.com/pinage404/pinage404-vscode-extension-packs/-/merge_requests
[issue-url]: https://gitlab.com/pinage404/pinage404-vscode-extension-packs/-/issues
