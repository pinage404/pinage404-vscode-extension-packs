# pinage404's VSCode Extension Packs

The purpose of this repository is to provide useful extension packs : hand-picked and carefully crafted to meet the needs

Each extension pack :

-   should only contain elements related to the topic of the pack
-   should contain elements for general use
    -   not related to a specific language, framework, platform or service
    -   unless that is the main purpose of the pack

Extensions that work out of the box (without relying on separately installed components) should be preferred

-   unless
    -   there is no equivalent alternative
    -   the advantages are enormous

## Release

Todo list before releasing a package

-   add / write / check files :
    -   `package.json`
        -   [ ] `name`
        -   [ ] `displayName`
        -   [ ] `description`
        -   [ ] `keywords`
        -   [ ] `version`
        -   [ ] `preview` optional
        -   [ ] `icon` optional while `preview`
        -   [ ] `galleryBanner`
        -   [ ] `categories`
        -   [ ] `extensionPack`
        -   [ ] `engines` (VSCode's version)
        -   [ ] global metadata
        -   [ ] `homepage`
    -   [ ] `README.md`
    -   [ ] `LICENSE`
    -   [ ] `CHANGELOG.md` optional while `preview`
    -   [ ] `icon.png` optional while `preview`
    -   [ ] `.gitignore` optional
    -   [ ] `.vscodeignore` optional
