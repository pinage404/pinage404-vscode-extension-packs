---
title: Markdown extensions test
draft: true
---

# Markdown extensions test

-   [Links](#links)
-   [Semantic](#semantic)
-   [Todo list](#todo-list)
-   [Code snippet](#code-snippet)
-   [Table](#table)
-   [Footnote](#footnote)

## Links

[Unknown link](unknown)

[Relative link](./README.md)

## Semantic

**Bold** _empashed_ ~~strikethrough~~

## Todo list

-   [x] done
-   [ ] still to do

## Code snippet

```bash
echo "simple command"
```

## Table

| Left     | center | right |
| :------- | :----: | ----: |
| Markdown |   is   |  cool |
| Ok       |   👍   |     9 |

## Footnote

Blabla [^1]

[^1]: Some foot note
