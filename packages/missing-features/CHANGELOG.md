## V1.3.0 / 2021-08-22

-   Rename extension from "VSCode's Missing Features Extension Pack" to "Missing Features of VS Code Extension Pack", better for small sidebar

## V1.2.0 / 2021-08-22

-   Rename extension from "General Improvement Extension Pack" to "VSCode's Missing Features Extension Pack"

## V1.1.0 / 2021-08-17

-   Add extension [Path Intellisense](https://marketplace.visualstudio.com/items?itemName=christian-kohler.path-intellisense) : Visual Studio Code plugin that autocompletes filenames

## V1.0.0 / 2021-08-15

-   Move repository
-   Change `extensionDependencies` to `extensionPack`
-   Replace [Presentation Mode](https://marketplace.visualstudio.com/items?itemName=jspolancor.presentationmode) by [Custom Presentation Mode](https://marketplace.visualstudio.com/items?itemName=jdsteinbach.custom-presentation-mode) : because it's more customisable
-   Remove [Partial Diff](https://marketplace.visualstudio.com/items?itemName=ryu1kn.partial-diff) : it could be util, but i never used it, i usually play with Git and the gutters color

## v0.2.1 / 2018-04-05

-   Upd: simpler icon

## v0.2.0 / 2018-04-04

-   Add: icon
-   Fix: repository URL in README

## v0.1.0 / 2018-04-01

-   First release
