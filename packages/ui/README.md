# pinage404's User Interface VS Code Extension Pack

Opinionated extension pack to customize and improve user interface (technology and language agnostic)

[![Visual Studio Marketplace Installs][vscode_marketplace_installs]][vscode_marketplace]
[![Open VSX Downloads][open_vsx_downloads]][open_vsx]

[vscode_marketplace_installs]: https://img.shields.io/visual-studio-marketplace/i/pinage404.user-interface-extension-pack?label=VSCode%20Marketplace%20Installs
[vscode_marketplace]: https://marketplace.visualstudio.com/items?itemName=pinage404.user-interface-extension-pack
[open_vsx_downloads]: https://img.shields.io/open-vsx/dt/pinage404/user-interface-extension-pack?label=Open%20VSX%20Registry%20Downloads
[open_vsx]: https://open-vsx.org/extension/pinage404/user-interface-extension-pack
