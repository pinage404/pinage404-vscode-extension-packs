## V1.3.0 / 2024-08-31

-   Remove [HTML Snippets](https://marketplace.visualstudio.com/items?itemName=abusaidm.html-snippets) because it is deprecated in favor of the native VSCode built-in extension ; Thanks [@yashpalgoyal1304 (Teen)](https://gitlab.com/yashpalgoyal1304) for the contribution
-   Replace [Kenneth Auchenberg's `Browser Preview`](https://marketplace.visualstudio.com/items?itemName=auchenberg.vscode-browser-preview) by [Anthony Fu's `Browser Lite`](https://marketplace.visualstudio.com/items?itemName=HTMLHint.vscode-htmlhint) ; Thanks [@yashpalgoyal1304 (Teen)](https://gitlab.com/yashpalgoyal1304) for pointing this out
    -   The former is depreciated in favor of [Microsoft's Live Preview](https://marketplace.visualstudio.com/items?itemName=ms-vscode.live-server)
    -   But the latter doesn't support any URL [see comments in #3](https://gitlab.com/pinage404/pinage404-vscode-extension-packs/-/issues/3#note_1666324073)

## V1.2.0 / 2022-11-14

-   Replace [Mike Kaufman's `HTMLHint`](https://marketplace.visualstudio.com/items?itemName=mkaufman.HTMLHint) by [HTMLHint's `HTMLHint`](https://marketplace.visualstudio.com/items?itemName=HTMLHint.vscode-htmlhint)
    -   The former is depreciated in favor of the latter

## V1.1.0 / 2021-08-22

-   change icon: blend icons :
    -   HTML5's logo from https://commons.wikimedia.org/wiki/File:HTML5_Badge.svg
    -   CSS3's logo from https://commons.wikimedia.org/wiki/File:CSS3_logo.svg

## V1.0.0 / 2021-08-22

-   Add: icon downloaded from https://commons.wikimedia.org/wiki/File:CSS3_and_HTML5_logos_and_wordmarks.svg

## v0.2.0 / 2021-08-15

-   Move repository
-   Change `extensionDependencies` to `extensionPack`
-   Add extensions
    -   [HTML Snippets](https://marketplace.visualstudio.com/items?itemName=abusaidm.html-snippets) : Full HTML tags including HTML5 Snippets
    -   [Browser Preview](https://marketplace.visualstudio.com/items?itemName=auchenberg.vscode-browser-preview) : A real browser preview inside your editor that you can debug
    -   [Name That Color](https://marketplace.visualstudio.com/items?itemName=guillaumedoutriaux.name-that-color) : Get a friendly name from a Hex color representation
    -   [Web Accessibility](https://marketplace.visualstudio.com/items?itemName=MaxvanderSchee.web-accessibility) : Audit Web Accessibility issues in Visual Studio Code
    -   [HTMLHint](https://marketplace.visualstudio.com/items?itemName=mkaufman.HTMLHint) : VS Code integration for HTMLHint - A Static Code Analysis Tool for HTML
    -   [Fabulous](https://marketplace.visualstudio.com/items?itemName=Raathigeshan.fabulous) : CSS editor
    -   [XML](https://marketplace.visualstudio.com/items?itemName=redhat.vscode-xml) : XML Language Support by Red Hat
    -   [CSSTree validator](https://marketplace.visualstudio.com/items?itemName=smelukov.vscode-csstree) : Validate CSS according to W3C specs and browser implementations
-   Replace [IntelliSense for CSS class names in HTML](https://marketplace.visualstudio.com/items?itemName=Zignd.html-css-class-completion) by [HTML Class Suggestions](https://marketplace.visualstudio.com/items?itemName=AndersEAndersen.html-class-suggestions) because handle class renaming without having to do manual action to refresh cache
-   Replace [Live Server](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer) by [Live Preview](https://marketplace.visualstudio.com/items?itemName=ms-vscode.live-server) because it was unmaintained
-   Remove [Auto Complete Tag](https://marketplace.visualstudio.com/items?itemName=formulahendry.auto-complete-tag) because, now, VSCode handles it by default

## v0.1.0 / 2018-04-01

-   First release
