# Better Readability VS Code Extension Pack

Ever wanted to be a superhero? You will see the invisible

Collection of extensions to improve readability

[![Visual Studio Marketplace Installs][vscode_marketplace_installs]][vscode_marketplace]
[![Open VSX Downloads][open_vsx_downloads]][open_vsx]

## Screenshot

### With

![screenshot with][screenshot_with]

### Without

![screenshot without][screenshot_without]

## VS Code Config

In addition to this extension, it is recommended to set the VSCode options to:

-   display all white spaces by setting `editor.renderWhitespace` to `all`

    ```json
    {
    	"editor.renderControlCharacters": true,
    	"editor.renderWhitespace": "all"
    }
    ```

-   color the corresponding brackets by setting `editor.bracketPairColorization.enabled` to `true`

    ```json
    {
    	"editor.bracketPairColorization.enabled": true
    }
    ```

## Want to see more extension added?

Open a [MR][merge-request-url] or an [issue][issue-url] and i will to take a look

[screenshot_with]: https://gitlab.com/pinage404/pinage404-vscode-extension-packs/-/raw/main/packages/better-readability/screenshot_with.png
[screenshot_without]: https://gitlab.com/pinage404/pinage404-vscode-extension-packs/-/raw/main/packages/better-readability/screenshot_without.png
[vscode_marketplace_installs]: https://img.shields.io/visual-studio-marketplace/i/pinage404.better-readability-extension-pack?label=VSCode%20Marketplace%20Installs
[vscode_marketplace]: https://marketplace.visualstudio.com/items?itemName=pinage404.better-readability-extension-pack
[open_vsx_downloads]: https://img.shields.io/open-vsx/dt/pinage404/better-readability-extension-pack?label=Open%20VSX%20Registry%20Downloads
[open_vsx]: https://open-vsx.org/extension/pinage404/better-readability-extension-pack
[merge-request-url]: https://gitlab.com/pinage404/pinage404-vscode-extension-packs/-/merge_requests
[issue-url]: https://gitlab.com/pinage404/pinage404-vscode-extension-packs/-/issues
