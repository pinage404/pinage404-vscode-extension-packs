## V1.6.0 / 2021-10-03

-   Add: [Rust Extension Pack](https://marketplace.visualstudio.com/items?itemName=pinage404.rust-extension-pack)

## V1.5.0 / 2021-09-18

-   Add: [More Missing Features of VS Code Extension Pack](https://marketplace.visualstudio.com/items?itemName=pinage404.more-missing-features-extension-pack)

## V1.4.0 / 2021-09-12

-   Add: [Common Configuration Languages Extension Pack](https://marketplace.visualstudio.com/items?itemName=pinage404.common-configuration-languages-extension-pack)

## V1.3.1 / 2021-09-12

-   Fix typo in [Markdown Extension Pack](https://marketplace.visualstudio.com/items?itemName=pinage404.markdown-extension-pack)

## V1.3.0 / 2021-09-12

-   Add: [Markdown Extension Pack](https://marketplace.visualstudio.com/items?itemName=pinage404.markdown-extension-pack)

## V1.2.0 / 2021-08-26

-   Add: [Bash Extension Pack](https://marketplace.visualstudio.com/items?itemName=pinage404.bash-extension-pack)

## V1.1.0 / 2021-08-22

-   Add: [Nix Extension Pack](https://marketplace.visualstudio.com/items?itemName=pinage404.nix-extension-pack)

## V1.0.0 / 2021-08-20

-   First major release
