# Bash VS Code Extension Pack

Opinionated extension pack to improve Bash usage

[![Visual Studio Marketplace Installs][vscode_marketplace_installs]][vscode_marketplace]
[![Open VSX Downloads][open_vsx_downloads]][open_vsx]

## Requirements

### Shell Format

To enjoy the [shell-format](https://marketplace.visualstudio.com/items?itemName=foxundermoon.shell-format) extension, [`shfmt`](https://github.com/mvdan/sh) must be installed

### DirEnv

To enjoy the [direnv](https://marketplace.visualstudio.com/items?itemName=mkhl.direnv) extension, follow the [direnv](https://direnv.net/) installation procedure

## Want to see more extension added?

Open a [MR][merge-request-url] or an [issue][issue-url] and i will to take a look

[vscode_marketplace_installs]: https://img.shields.io/visual-studio-marketplace/i/pinage404.bash-extension-pack?label=VSCode%20Marketplace%20Installs
[vscode_marketplace]: https://marketplace.visualstudio.com/items?itemName=pinage404.bash-extension-pack
[open_vsx_downloads]: https://img.shields.io/open-vsx/dt/pinage404/bash-extension-pack?label=Open%20VSX%20Registry%20Downloads
[open_vsx]: https://open-vsx.org/extension/pinage404/bash-extension-pack
[merge-request-url]: https://gitlab.com/pinage404/pinage404-vscode-extension-packs/-/merge_requests
[issue-url]: https://gitlab.com/pinage404/pinage404-vscode-extension-packs/-/issues
